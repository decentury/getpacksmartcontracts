pragma solidity ^0.4.15;

import '../lib/SafeMath.sol';
import '../lib/Base.sol';
import '../lib/Owned.sol';
import '../token/TokenTimeLock.sol';


contract Crowdsale is Base, Owned {
    using SafeMath for uint256;

    enum State { INIT, ICO, CLOSED, STOPPED }
    enum SupplyType { BOUNTY, ICO }

    uint public constant DECIMALS = 10**18;

    uint public constant MAX_ICO_SUPPLY = 70000000 * DECIMALS;
    uint public constant MAX_BOUNTY_SUPPLY = 2000000 * DECIMALS;

    uint public constant EXOFUND_TOKENS = 13000000 * DECIMALS;
    uint public constant FIRST_TOKENS_FOR_RESERVE = 1750000 * DECIMALS;
    uint public constant SECOND_TOKENS_FOR_RESERVE = 1750000 * DECIMALS;
    uint public constant TEAM_TOKENS_FOR_HALF_YEAR = 7500000 * DECIMALS;
    uint public constant TEAM_TOKENS_FOR_YEAR = 7500000 * DECIMALS;

    State public currentState = State.INIT;
    IToken public token;

    uint public totalICOSupply = 0;
    uint public totalBountySupply = 0;

    TokenTimelock public lockedTeamTokensForHalfYear;
    TokenTimelock public lockedTeamTokensForYear;
    TokenTimelock public firstReserveLockedTeamTokens;
    TokenTimelock public secondReserveLockedTeamTokens;

    event Mint(address indexed _to, uint256 _value);
    event Bounty(address indexed _to, uint256 _value);

    modifier inState(State _state){
        require(currentState == _state);
        _;
    }

    modifier salesRunning(){
        require(currentState == State.ICO);
        _;
    }

    modifier notStopped(){
        require(currentState != State.STOPPED);
        _;
    }

    function Crowdsale() public {
    }

    function ()
        public
        payable
        salesRunning
    {
        revert();
    }

    function initialize(address _token, address exofund, address _lockedTeamAddressForHalfYear, address _lockedTeamAddressForYear)
        public
        onlyOwner
        inState(State.INIT)
    {
        require(_token != address(0));

        token = IToken(_token);
        IToken(token).mint(exofund, EXOFUND_TOKENS);

        sendTeamTokens(_lockedTeamAddressForHalfYear, _lockedTeamAddressForYear);
    }

    function setState(State _newState)
        public
        onlyOwner
    {
        require(
            (currentState == State.INIT && _newState == State.ICO)
            || (currentState == State.ICO && _newState == State.CLOSED)
            || (currentState == State.ICO && _newState == State.STOPPED)
            || (currentState == State.STOPPED && _newState == State.ICO)
        );
        if(_newState == State.CLOSED){
            _finish();
        }

        currentState = _newState;
    }

    function sendTokens(address _to, uint _amount)
        public
        onlyOwner
        salesRunning
    {
        _amount = _amount.mul(DECIMALS);
        _checkMaxSaleSupply(_amount, SupplyType.ICO);
        _mint(_to, _amount, SupplyType.ICO);
    }

    function setFirstReserveAddress(address addr)
        public
        onlyOwner
    {
        firstReserveLockedTeamTokens.setAddress(addr);
    }

    function setSecondReserveAddress(address addr)
        public
        onlyOwner
    {
        secondReserveLockedTeamTokens.setAddress(addr);
    }

    function sendBounty(address _to, uint _amount)
        public
        onlyOwner
        salesRunning
    {
        _amount = _amount.mul(DECIMALS);
        _checkMaxSaleSupply(_amount, SupplyType.BOUNTY);
        _mint(_to, _amount, SupplyType.BOUNTY);
    }


    //==================== Internal Methods =================

    function sendTeamTokens(address _lockedTeamAddressForHalfYear, address _lockedTeamAddressForYear)
        noAnyReentrancy
        internal
    {
        require(lockedTeamTokensForHalfYear == address(0));
        require(lockedTeamTokensForYear == address(0));
        require(_lockedTeamAddressForHalfYear != address(0));
        require(_lockedTeamAddressForYear != address(0));

        uint64 currentTime = (uint64)(now);
        firstReserveLockedTeamTokens = new TokenTimelock(token, address(0), currentTime + 60 days);
        secondReserveLockedTeamTokens = new TokenTimelock(token, address(0), currentTime + 183 days);
        lockedTeamTokensForHalfYear = new TokenTimelock(token, _lockedTeamAddressForHalfYear, currentTime + 183 days);
        lockedTeamTokensForYear = new TokenTimelock(token, _lockedTeamAddressForYear, currentTime + 365 days);

        IToken(token).mint(lockedTeamTokensForHalfYear, TEAM_TOKENS_FOR_HALF_YEAR);
        IToken(token).mint(lockedTeamTokensForYear, TEAM_TOKENS_FOR_YEAR);
        IToken(token).mint(firstReserveLockedTeamTokens, FIRST_TOKENS_FOR_RESERVE);
        IToken(token).mint(secondReserveLockedTeamTokens, SECOND_TOKENS_FOR_RESERVE);
    }

    function _mint(address _to, uint _amount, SupplyType _supplyType)
        noAnyReentrancy
        internal
    {
        _increaseSupply(_amount, _supplyType);
        IToken(token).mint(_to, _amount);
        if(_supplyType == SupplyType.ICO)
            Mint(_to, _amount);
        else
            Bounty(_to, _amount);
    }

    function _finish()
        noAnyReentrancy
        internal
    {
        IToken(token).start();
    }

    function _checkMaxSaleSupply(uint transferTokens, SupplyType _supplyType)
        internal
    {
        if(_supplyType == SupplyType.ICO) {
            require(totalICOSupply.add(transferTokens) <= MAX_ICO_SUPPLY);
        } else if(_supplyType == SupplyType.BOUNTY) {
            require(totalBountySupply.add(transferTokens) <= MAX_BOUNTY_SUPPLY);
        }
    }

    function _increaseSupply(uint _amount, SupplyType _supplyType)
        internal
    {
        if(_supplyType == SupplyType.ICO) {
            totalICOSupply = totalICOSupply.add(_amount);
        } else if(_supplyType == SupplyType.BOUNTY) {
            totalBountySupply = totalBountySupply.add(_amount);
        }
    }

}
