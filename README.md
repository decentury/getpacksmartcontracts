#### Тестирование контрактов
Установка testrpc
```console
npm install -g ethereumjs-testrpc@4.1.3
```
Установка truffle
```console
npm install -g truffle@3.4.9
```
запуск testrpc(запуск ноды с эфиром)
```console
testrpc > testrpc.output &
```
запуск тестов
```console
truffle test
```
#### Деплой контрактов
Установка solidity-flattener
```console
git clone https://github.com/poanetwork/solidity-flattener
cd solidity-flattener
npm install
```
Сбор всех зависимостей в один файл
```console
npm start "../contracts/crowdsale/Crowdsale.sol"
npm start "../contracts/token/Token.sol"
npm start "../contracts/token/TokenTimelock.sol"
```
Полученные файлы находятся в папке out

Загрузка файлов в [Ремикс](https://remix.ethereum.org/)

#####Получение байт-кода Crowdsale
1. Создать в Ремикс файл с именем crowdsale.sol
2. Скопировать содержимое файла из out/Crowdsale_flat.sol
3. Внизу "Start to compile" выбрать Crowdsale
4. Details
5. скопировать BYTECODE->object 

#####Получение байт-кода Token
1. Создать в Ремикс файл с именем token.sol
2. Скопировать содержимое файла из out/Token_flat.sol
3. Внизу "Start to compile" выбрать Token
4. Details
5. скопировать BYTECODE->object 

#####Получение байт-кода TokenTimelock
Аналогично Token, но с соотвествующей заменой имен

#####Деплой смарт-контрактов
1. Зайти на [myetherwallet](https://www.myetherwallet.com/)
2. Contracts->Deploy Contract
3. В Byte Code вставить байт-код нужного контракта(Token и Crowdsale)
4. Выбрать кошелек, который будет владельцем контрактов

#### Использование контрактов
#####Получение ABI
Шаги 1-4 эквивалентны получению байт-кода, но в 5 пункте скопировать ABI

#####Использование
1. Зайти на [myetherwallet](https://www.myetherwallet.com/)
2. Contracts->Interact with Contract
3. В Contract Address вставить адрес контракта
   В ABI / JSON Interface вставить ABI из предыдущего пункта
4. Access

#### Инструкция работы с контрактами

#####Инициализация
1. Задеплоить контракт Token и Crowdsale
2. У контракта Token вызвать метод setCrowdsaleMinter, указать в качестве аргумента адрес контракта Crowdsale
3. У контракта Crowdsale вызвать метод initialize, указать в качестве аргументов:  
   1 - адрес контракта Token  
   2 - адрес кошелька экзофонда  
   3 - адрес кошелька команды с заморозкой токенов на полгода  
   4 - адрес кошелька команды с заморозкой токенов на год  
4. Установить адреса для замороженных адресов на 2 и 6 месяцев:  
    У контракта Crowdsale вызвать метод setFirstReserveAddress, чтобы задать адрес, на который перечислится 1.75% токенов после 2-ух месяцев  
    У контракта Crowdsale вызвать метод setSecondReserveAddress, чтобы задать адрес, на который перечислится 1.75% токенов после полугода месяцев

#####Смена стадий
У контракта Crowdsale вызвать метод setState. В качестве аргумента указать цифру:  
1 - переключить на стадию ICO  
2 - завершить стадию ICO(токены больше эмитировать будет невозможно, токены становятся доступны для перевода внутри пользователей)  
3 - поставить ICO на паузу(если контракт в этой стадии, то его можно запустить послав аргумент "1")  

#####Разморозка средств
У контракта Crowdsale есть 4 контракта:
- lockedTeamTokensForHalfYear  
- lockedTeamTokensForYear;  
- firstReserveLockedTeamTokens;  
- secondReserveLockedTeamTokens;  
Чтобы получить средства с каждого контракта, нужно вызвать метод release у каждого контракта
ВНИМАНИЕ! Токены не смогут начислиться на кошелек, если ICO будет незакончено. т.е методы по разморозке токенов можно вызывать только после ICO!
#####Эмиссия
1. Эмиссия токенов из пула ICO - У контракта Crowdsale вызвать метод sendTokens, указать в качестве аргумента:  
   1 - адрес пользователя, которому зачисляются токены  
   2 - кол-во токенов(не нужно умножать на 10^18)  
2. Эмиссия токенов из пула Bounty - У контракта Crowdsale вызвать метод sendBounty, указать в качестве аргумента:  
   1 - адрес пользователя, которому зачисляются токены  
   2 - кол-во токенов(не нужно умножать на 10^18)  