const BigNumber = web3.BigNumber

function latestTime() {
  return web3.eth.getBlock('latest').timestamp;
}
function increaseTime(duration) {
  const id = Date.now()

  return new Promise((resolve, reject) => {
    web3.currentProvider.sendAsync({
      jsonrpc: '2.0',
      method: 'evm_increaseTime',
      params: [duration],
      id: id,
    }, err1 => {
      if (err1) return reject(err1)

      web3.currentProvider.sendAsync({
        jsonrpc: '2.0',
        method: 'evm_mine',
        id: id+1,
      }, (err2, res) => {
        return err2 ? reject(err2) : resolve(res)
      })
    })
  })
}

function increaseTimeTo(target) {
  let now = latestTime();
  if (target < now) throw Error(`Cannot increase current time(${now}) to a moment in the past(${target})`);
  let diff = target - now;
  return increaseTime(diff);
}

const duration = {
  seconds: function(val) { return val},
  minutes: function(val) { return val * this.seconds(60) },
  hours:   function(val) { return val * this.minutes(60) },
  days:    function(val) { return val * this.hours(24) },
  weeks:   function(val) { return val * this.days(7) },
  years:   function(val) { return val * this.days(365)}
};

const Token = artifacts.require('Token')
const TokenTimelock = artifacts.require('TokenTimelock')

contract('TokenTimelock', function ([_, owner, beneficiary]) {

  const amount = new BigNumber(100)

  beforeEach(async function () {
    this.token = await Token.new({from: owner})
    await this.token.setCrowdsaleMinter(owner, {from: owner})
    this.releaseTime = latestTime() + duration.years(1)
    this.timelock = await TokenTimelock.new(this.token.address, beneficiary, this.releaseTime)
    await this.token.mint(this.timelock.address, amount, {from: owner})
    await this.token.start({from: owner})
  })

  it('cannot be released before time limit', async function () {
    try {
      await this.timelock.release();
      assert.fail('should have thrown before');
    } catch(error) {
    }
  })

  it('cannot be released just before time limit', async function () {
    await increaseTimeTo(this.releaseTime - duration.seconds(3))
    try {
      await this.timelock.release();
      assert.fail('should have thrown before');
    } catch(error) {
    }
  })

  it('can be released just after limit', async function () {
    await increaseTimeTo(this.releaseTime + duration.seconds(1))
    await this.timelock.release()
    const balance = await this.token.balanceOf(beneficiary)
    assert.equal(balance.toNumber(), amount.toNumber(), "Can not be released");
  })

  it('can be released after time limit', async function () {
    await increaseTimeTo(this.releaseTime + duration.years(1))
    await this.timelock.release()
    const balance = await this.token.balanceOf(beneficiary)
    assert.equal(balance.toNumber(), amount.toNumber(), "Can not be released");
  })

  it('cannot be released twice', async function () {
    await increaseTimeTo(this.releaseTime + duration.years(1))
    await this.timelock.release()
    try {
      await this.timelock.release();
      assert.fail('should have thrown before');
    } catch(error) {
    }
    const balance = await this.token.balanceOf(beneficiary)
    assert.equal(balance.toNumber(), amount.toNumber(), "Can not be released");
  })

})
