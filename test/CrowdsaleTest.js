const Crowdsale = artifacts.require('Crowdsale');
const Token = artifacts.require('Token');
const TokenTimelock = artifacts.require('TokenTimelock')

const BigNumber = web3.BigNumber

function latestTime() {
  return web3.eth.getBlock('latest').timestamp;
}
function increaseTime(duration) {
  const id = Date.now()

  return new Promise((resolve, reject) => {
    web3.currentProvider.sendAsync({
      jsonrpc: '2.0',
      method: 'evm_increaseTime',
      params: [duration],
      id: id,
    }, err1 => {
      if (err1) return reject(err1)

      web3.currentProvider.sendAsync({
        jsonrpc: '2.0',
        method: 'evm_mine',
        id: id+1,
      }, (err2, res) => {
        return err2 ? reject(err2) : resolve(res)
      })
    })
  })
}

function increaseTimeTo(target) {
  let now = latestTime();
  return increaseTime(target);
}

const duration = {
  seconds: function(val) { return val},
  minutes: function(val) { return val * this.seconds(60) },
  hours:   function(val) { return val * this.minutes(60) },
  days:    function(val) { return val * this.hours(24) },
  weeks:   function(val) { return val * this.days(7) },
  years:   function(val) { return val * this.days(365)}
};


contract('Crowdsale', function (accounts) {
  let investor = accounts[0];
  let crowdsaleOwner = accounts[1];
  let tokenOwner = accounts[2];
  let account = accounts[3];
  let exofund = accounts[4];
  let teamsHalf = accounts[5];
  let teamsRest = accounts[6];
  let token;
  let crowdsale;

  const DECIMALS = 1000000000000000000;

  async function checkContractOwner(contractInstance, expectedOwner){
    let ownerCall = await contractInstance.owner.call();
    assert.equal(ownerCall.valueOf(), expectedOwner, 'The owner in the contract does not match the value that is set in the settings');
  };

  async function setState(state){
    let currentState = await crowdsale.currentState();
    for(let i = currentState.toNumber() + 1; i <= state; i++) {
      await crowdsale.setState(i, {from: crowdsaleOwner});
    }
    currentState = await crowdsale.currentState();
    assert.equal(currentState.toNumber(), state);
  };

  async function checkCallFnNotOwner(fn, nameFn, nameOwner, ...args) {
    try {
      await fn(...args, {from: account});

      assert.fail(0, 0, "{0} can be called not by {1}".format(nameFn, nameOwner));
    } catch (e){
    };
  }

  async function checkCallFnInState(fn, nameFn, state, ...args) {
    try {
      await fn(...args, {from: account});

      assert.fail(0, 0, "{0} can be called in state {1}".format(nameFn, state));
    } catch (error){
      const invalidOpcode = error.message.search('invalid opcode') >= 0;
      const outOfGas = error.message.search('out of gas') >= 0;
      assert(
        invalidOpcode || outOfGas,
        "Expected throw, got '" + error + "' instead",
      );
    };
  }

  async function checkCallFnInStateFromOwner(fn, nameFn, state, ...args) {
    try {
      await fn(...args, {from: crowdsaleOwner});

      assert.fail(0, 0, "{0} can be called in state {1}".format(nameFn, state));
    } catch (error){
      const invalidOpcode = error.message.search('invalid opcode') >= 0;
      const outOfGas = error.message.search('out of gas') >= 0;
      assert(
        invalidOpcode || outOfGas,
        "Expected throw, got '" + error + "' instead",
      );
    };
  }

  async function checkCallInternalFn(fn, nameFn, ...args) {
    try {
      await fn(...args, {from: account});

      assert.fail(0, 0, "{0} is not internal function".format(nameFn));
    } catch (e){
    };
  }


  beforeEach(async function () {
    token = await Token.new({from: tokenOwner});
    crowdsale = await Crowdsale.new({from: crowdsaleOwner});
    await token.setCrowdsaleMinter(crowdsale.address, {from: tokenOwner});
    await crowdsale.initialize(token.address, exofund, teamsHalf, teamsRest, {from: crowdsaleOwner});
  });

  it('the contract owner must be correctly set', async function() {
    await checkContractOwner(crowdsale, crowdsaleOwner);
    await checkContractOwner(token, tokenOwner);
  });

  it('token minter must be set correctly', async function () {
    const minter = await token.crowdsaleMinter();
    assert.equal(minter, crowdsale.address);
  });

  it('token must be set correctly', async function () {
    const crowdsaleToken = await crowdsale.token();
    assert.equal(crowdsaleToken, token.address);
  });

  it('_mint, _finish, _checkMaxSaleSupply must be internal function', async function(){
    await checkCallInternalFn(crowdsale._mint, 'crowdsale._mint', 0, investor, 100);
    await checkCallInternalFn(crowdsale._finish, 'crowdsale._finish', 0);
    await checkCallInternalFn(crowdsale._checkMaxSaleSupply, 'crowdsale._checkMaxSaleSupply', 0);
  });

  it('should set correct new state', async function () {
    let state = await crowdsale.currentState();
    assert.equal(state, 0);

    for(let i = 1; i < 3; i++) {
      await crowdsale.setState(i, {from: crowdsaleOwner});
      state = await crowdsale.currentState();
      assert.equal(state, i);
    }
  });

  it('should not set new state', async function () {
    let state = await crowdsale.currentState();
    assert.equal(state, 0);

    for(let i = 1; i < 2; i++) {
      try {
        await crowdsale.setState(i + 1, {from: crowdsaleOwner});
      } catch (error) {
        const invalidOpcode = error.message.search('invalid opcode') >= 0;
        const outOfGas = error.message.search('out of gas') >= 0;
        assert(
          invalidOpcode || outOfGas,
          "Expected throw, got '" + error + "' instead",
        );
      }
      await crowdsale.setState(i, {from: crowdsaleOwner});
    }
  });

  it('should set STOPPED state', async function () {
    let state = await crowdsale.currentState();
    assert.equal(state, 0);

    await crowdsale.setState(1, {from: crowdsaleOwner});
    state = await crowdsale.currentState();
    assert.equal(state, 1);

    await crowdsale.setState(3, {from: crowdsaleOwner});
    state = await crowdsale.currentState();
    assert.equal(state, 3);

    await crowdsale.setState(1, {from: crowdsaleOwner});
    state = await crowdsale.currentState();
    assert.equal(state, 1);
  });

//--------------------------------------------------------------------------

  it('Send team half tokens', async function() {
    await setState(1);

    let lockedTeamTokens = await crowdsale.lockedTeamTokensForHalfYear();
    let lockedBalance = await token.balanceOf(lockedTeamTokens);
    assert.equal(lockedBalance.toNumber(), '7500000000000000000000000', "Tokens are not credited to the team's locked balance");

    let currentLockedBalance = await token.balanceOf(teamsHalf);
    assert.equal(currentLockedBalance.toNumber(), 0, "Tokens are not locked");
  });

  it('Send team rest tokens', async function() {
    await setState(1);

    let lockedTeamTokens = await crowdsale.lockedTeamTokensForYear();
    let lockedBalance = await token.balanceOf(lockedTeamTokens);
    assert.equal(lockedBalance.toNumber(), '7500000000000000000000000', "Tokens are not credited to the team's locked balance");

    let currentLockedBalance = await token.balanceOf(teamsRest);
    assert.equal(currentLockedBalance.toNumber(), 0, "Tokens are not locked");
  });

  it('Test exofund tokens', async function() {
    await setState(1);

    let balance = await token.balanceOf(exofund);
    assert.equal(balance, 13000000 * DECIMALS, "Tokens are not credited to the user's balance");
  });


  it('Send tokens to investors', async function() {
    await setState(1);

    let amount = 10;
    let currentBalance = 0;
    
    await crowdsale.sendTokens(investor, amount, {from: crowdsaleOwner});
    currentBalance += amount;

    let balance = await token.balanceOf(investor);
    assert.equal(balance, currentBalance * DECIMALS, "Tokens are not credited to the user's balance");
    totalICOSupply = await crowdsale.totalICOSupply();
    assert.equal(totalICOSupply.toNumber(), amount * DECIMALS, 'Tokens were not added to totalICOSupply');
    totalBountySupply = await crowdsale.totalBountySupply();
    assert.equal(totalBountySupply.toNumber(), 0, 'Tokens were added to totalBountySupply');
    
    await crowdsale.sendTokens(investor, amount, {from: crowdsaleOwner});
    currentBalance += amount;
    balance = await token.balanceOf(investor);
    assert.equal(balance, currentBalance * DECIMALS, "Tokens are not credited to the user's balance");
    totalICOSupply = await crowdsale.totalICOSupply();
    assert.equal(totalICOSupply.toNumber(), currentBalance * DECIMALS, 'Tokens were not added to totalICOSupply');
    totalBountySupply = await crowdsale.totalBountySupply();
    assert.equal(totalBountySupply.toNumber(), 0, 'Tokens were not added to totalBountySupply');
  });

  it('Send bounty tokens to investors', async function() {
    await setState(1);

    let amount = 10;
    let currentBalance = 0;
    
    await crowdsale.sendBounty(investor, amount, {from: crowdsaleOwner});
    currentBalance += amount;

    let balance = await token.balanceOf(investor);
    assert.equal(balance, currentBalance * DECIMALS, "Tokens are not credited to the user's balance");
    totalICOSupply = await crowdsale.totalICOSupply();
    assert.equal(totalICOSupply.toNumber(), 0, 'Tokens were not added to totalICOSupply');
    totalBountySupply = await crowdsale.totalBountySupply();
    assert.equal(totalBountySupply.toNumber(), amount * DECIMALS, 'Tokens were added to totalBountySupply');
    
    await crowdsale.sendBounty(investor, amount, {from: crowdsaleOwner});
    currentBalance += amount;
    balance = await token.balanceOf(investor);
    assert.equal(balance, currentBalance * DECIMALS, "Tokens are not credited to the user's balance");
    totalICOSupply = await crowdsale.totalICOSupply();
    assert.equal(totalICOSupply.toNumber(), 0, 'Tokens were not added to totalICOSupply');
    totalBountySupply = await crowdsale.totalBountySupply();
    assert.equal(totalBountySupply.toNumber(), currentBalance * DECIMALS, 'Tokens were not added to totalBountySupply');
  });


  //--------------------------Total supply limits tests-----------------------------------------------

  it("Limit of ICO innvestment totalPresaleSupply", async function() {
    await setState(1);
    await crowdsale.sendTokens(investor, 70000000, {from: crowdsaleOwner});
    //Trying to exceed the totalPresaleSupply
    try {
    await crowdsale.sendTokens(investor, 1, {from: crowdsaleOwner});
      assert.fail(0, 0, "totalICOSupply can be exceeded");
    } catch (error) {
      const invalidOpcode = error.message.search('invalid opcode') >= 0;
      const outOfGas = error.message.search('out of gas') >= 0;
      assert(
        invalidOpcode || outOfGas,
        "Expected throw, got '" + error + "' instead",
      );
    }
  });


  it("Limit of Bounty innvestment", async function() {
    await setState(1);
    await crowdsale.sendBounty(investor, 2000000, {from: crowdsaleOwner});
    //Trying to exceed the totalPresaleSupply
    try {
    await crowdsale.sendBounty(investor, 1, {from: crowdsaleOwner});
      assert.fail(0, 0, "totalBountySupply can be exceeded");
    } catch (error) {
      const invalidOpcode = error.message.search('invalid opcode') >= 0;
      const outOfGas = error.message.search('out of gas') >= 0;
      assert(
        invalidOpcode || outOfGas,
        "Expected throw, got '" + error + "' instead",
      );
    }
  });
  // -------------------Reserve token test

  it('lockedTeamTokensForHalfYear cannot be released before time limit', async function () {
    await setState(1);
    await setState(2);
    try {
      let lockedTeamTokens = await TokenTimelock.at(await crowdsale.lockedTeamTokensForHalfYear()); 
      
      let balance = await token.balanceOf(teamsHalf);
      assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
      balance = await token.balanceOf(await crowdsale.lockedTeamTokensForHalfYear());
      assert.equal(balance.toNumber(), 7500000 * DECIMALS, "Can not be released");
  
      await lockedTeamTokens.release({from: crowdsaleOwner});
  
      balance = await token.balanceOf(teamsHalf);
      assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
      balance = await token.balanceOf(await crowdsale.lockedTeamTokensForHalfYear());
      assert.equal(balance.toNumber(), 7500000 * DECIMALS, "Can not be released");
      
      assert.fail('should have thrown before');
    } catch(error) {
    }
  })

  it('lockedTeamTokensForHalfYear can be released after time limit', async function () {
    await setState(1);
    await setState(2);
    await increaseTimeTo(duration.years(0.6))
    let lockedTeamTokens = await TokenTimelock.at(await crowdsale.lockedTeamTokensForHalfYear()); 

    let balance = await token.balanceOf(teamsHalf);
    assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
    balance = await token.balanceOf(await crowdsale.lockedTeamTokensForHalfYear());
    assert.equal(balance.toNumber(), 7500000 * DECIMALS, "Can not be released");

    await lockedTeamTokens.release({from: crowdsaleOwner});

    balance = await token.balanceOf(teamsHalf);
    assert.equal(balance.toNumber(), 7500000 * DECIMALS, "Can not be released");
    balance = await token.balanceOf(await crowdsale.lockedTeamTokensForHalfYear());
    assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
  })

  it('lockedTeamTokensForYear cannot be released before time limit', async function () {
    await setState(1);
    await setState(2);
    try {
      let lockedTeamTokens = await TokenTimelock.at(await crowdsale.lockedTeamTokensForYear()); 
      
      let balance = await token.balanceOf(teamsRest);
      assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
      balance = await token.balanceOf(await crowdsale.lockedTeamTokensForYear());
      assert.equal(balance.toNumber(), 7500000 * DECIMALS, "Can not be released");
  
      await lockedTeamTokens.release({from: crowdsaleOwner});
  
      balance = await token.balanceOf(teamsRest);
      assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
      balance = await token.balanceOf(await crowdsale.lockedTeamTokensForYear());
      assert.equal(balance.toNumber(), 7500000 * DECIMALS, "Can not be released");
      
      assert.fail('should have thrown before');
    } catch(error) {
    }
  })

  it('lockedTeamTokensForYear cannot be released after time limit', async function () {
    await setState(1);
    await setState(2);
    await increaseTimeTo(duration.years(0.6))
    try {
      let lockedTeamTokens = await TokenTimelock.at(await crowdsale.lockedTeamTokensForYear()); 

      let balance = await token.balanceOf(teamsRest);
      assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
      balance = await token.balanceOf(await crowdsale.lockedTeamTokensForYear());
      assert.equal(balance.toNumber(), 7500000 * DECIMALS, "Can not be released");

      await lockedTeamTokens.release({from: crowdsaleOwner});

      balance = await token.balanceOf(teamsRest);
      assert.equal(balance.toNumber(), 7500000 * DECIMALS, "Can not be released");
      balance = await token.balanceOf(await crowdsale.lockedTeamTokensForYear());
      assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
      assert.fail('should have thrown before');
  } catch(error) {
  }
})

  it('lockedTeamTokensForYear can be released after time limit', async function () {
    await setState(1);
    await setState(2);
    await increaseTimeTo(duration.years(1.2))
    let lockedTeamTokens = await TokenTimelock.at(await crowdsale.lockedTeamTokensForYear()); 

    let balance = await token.balanceOf(teamsRest);
    assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
    balance = await token.balanceOf(await crowdsale.lockedTeamTokensForYear());
    assert.equal(balance.toNumber(), 7500000 * DECIMALS, "Can not be released");

    await lockedTeamTokens.release({from: crowdsaleOwner});

    balance = await token.balanceOf(teamsRest);
    assert.equal(balance.toNumber(), 7500000 * DECIMALS, "Can not be released");
    balance = await token.balanceOf(await crowdsale.lockedTeamTokensForYear());
    assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
  })

  it('firstReserveLockedTeamTokens cannot be released before time limit', async function () {
    await setState(1);
    await setState(2);
    let beneficiar = accounts[7];
    await crowdsale.setFirstReserveAddress(beneficiar, {from: crowdsaleOwner});
    try {
      let lockedTeamTokens = await TokenTimelock.at(await crowdsale.firstReserveLockedTeamTokens()); 
      let balance = await token.balanceOf(beneficiar);
      assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
      balance = await token.balanceOf(await crowdsale.firstReserveLockedTeamTokens());
      assert.equal(balance.toNumber(), 1750000 * DECIMALS, "Can not be released");
  
      await lockedTeamTokens.release({from: crowdsaleOwner});
  
      balance = await token.balanceOf(beneficiar);
      assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
      balance = await token.balanceOf(await crowdsale.firstReserveLockedTeamTokens());
      assert.equal(balance.toNumber(), 1750000 * DECIMALS, "Can not be released");
      
      assert.fail('should have thrown before');
    } catch(error) {
    }
  })

  it('firstReserveLockedTeamTokens cannot be released before time limit, and can be released after time with second attempt', async function () {
    await setState(1);
    await setState(2);
    let beneficiar = accounts[7];
    await crowdsale.setFirstReserveAddress(beneficiar, {from: crowdsaleOwner});
    try {
      let lockedTeamTokens = await TokenTimelock.at(await crowdsale.firstReserveLockedTeamTokens()); 
      let balance = await token.balanceOf(beneficiar);
      assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
      balance = await token.balanceOf(await crowdsale.firstReserveLockedTeamTokens());
      assert.equal(balance.toNumber(), 1750000 * DECIMALS, "Can not be released");
  
      await lockedTeamTokens.release({from: crowdsaleOwner});
  
      balance = await token.balanceOf(beneficiar);
      assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
      balance = await token.balanceOf(await crowdsale.firstReserveLockedTeamTokens());
      assert.equal(balance.toNumber(), 1750000 * DECIMALS, "Can not be released");
      assert.fail('should have thrown before');
    } catch(error) {
    }
    await increaseTimeTo(duration.years(0.3))
    let lockedTeamTokens = await TokenTimelock.at(await crowdsale.firstReserveLockedTeamTokens()); 
    await crowdsale.setFirstReserveAddress(beneficiar, {from: crowdsaleOwner});
    
    let balance = await token.balanceOf(beneficiar);
    assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
    balance = await token.balanceOf(await crowdsale.firstReserveLockedTeamTokens());
    assert.equal(balance.toNumber(), 1750000 * DECIMALS, "Can not be released");

    await lockedTeamTokens.release({from: crowdsaleOwner});

    balance = await token.balanceOf(beneficiar);
    assert.equal(balance.toNumber(), 1750000 * DECIMALS, "Can not be released");
    balance = await token.balanceOf(await crowdsale.firstReserveLockedTeamTokens());
    assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");

  })

  it('firstReserveLockedTeamTokens can be released after time limit', async function () {
    await setState(1);
    await setState(2);
    await increaseTimeTo(duration.years(0.3))
    let beneficiar = accounts[7];
    let lockedTeamTokens = await TokenTimelock.at(await crowdsale.firstReserveLockedTeamTokens()); 
    await crowdsale.setFirstReserveAddress(beneficiar, {from: crowdsaleOwner});
    
    let balance = await token.balanceOf(beneficiar);
    assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
    balance = await token.balanceOf(await crowdsale.firstReserveLockedTeamTokens());
    assert.equal(balance.toNumber(), 1750000 * DECIMALS, "Can not be released");

    await lockedTeamTokens.release({from: crowdsaleOwner});

    balance = await token.balanceOf(beneficiar);
    assert.equal(balance.toNumber(), 1750000 * DECIMALS, "Can not be released");
    balance = await token.balanceOf(await crowdsale.firstReserveLockedTeamTokens());
    assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
  })

  it('secondReserveLockedTeamTokens cannot be released before time limit', async function () {
    await setState(1);
    await setState(2);
    let beneficiar = accounts[7];
    await crowdsale.setSecondReserveAddress(beneficiar, {from: crowdsaleOwner});
    try {
      let lockedTeamTokens = await TokenTimelock.at(await crowdsale.secondReserveLockedTeamTokens()); 
      let balance = await token.balanceOf(beneficiar);
      assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
      balance = await token.balanceOf(await crowdsale.secondReserveLockedTeamTokens());
      assert.equal(balance.toNumber(), 1750000 * DECIMALS, "Can not be released");
  
      await lockedTeamTokens.release({from: crowdsaleOwner});
  
      balance = await token.balanceOf(beneficiar);
      assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
      balance = await token.balanceOf(await crowdsale.secondReserveLockedTeamTokens());
      assert.equal(balance.toNumber(), 1750000 * DECIMALS, "Can not be released");
      
      assert.fail('should have thrown before');
    } catch(error) {
    }
  })

  it('secondReserveLockedTeamTokens cannot be released before time limit, and can be released after time with second attempt', async function () {
    await setState(1);
    await setState(2);
    let beneficiar = accounts[7];
    await crowdsale.setSecondReserveAddress(beneficiar, {from: crowdsaleOwner});
    try {
      let lockedTeamTokens = await TokenTimelock.at(await crowdsale.secondReserveLockedTeamTokens()); 
      let balance = await token.balanceOf(beneficiar);
      assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
      balance = await token.balanceOf(await crowdsale.secondReserveLockedTeamTokens());
      assert.equal(balance.toNumber(), 1750000 * DECIMALS, "Can not be released");
  
      await lockedTeamTokens.release({from: crowdsaleOwner});
  
      balance = await token.balanceOf(beneficiar);
      assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
      balance = await token.balanceOf(await crowdsale.secondReserveLockedTeamTokens());
      assert.equal(balance.toNumber(), 1750000 * DECIMALS, "Can not be released");
      
      assert.fail('should have thrown before');
    } catch(error) {
    }
    await increaseTimeTo(duration.years(0.6))
    let lockedTeamTokens = await TokenTimelock.at(await crowdsale.secondReserveLockedTeamTokens()); 
    await crowdsale.setSecondReserveAddress(beneficiar, {from: crowdsaleOwner});
    
    let balance = await token.balanceOf(beneficiar);
    assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
    balance = await token.balanceOf(await crowdsale.secondReserveLockedTeamTokens());
    assert.equal(balance.toNumber(), 1750000 * DECIMALS, "Can not be released");

    await lockedTeamTokens.release({from: crowdsaleOwner});

    balance = await token.balanceOf(beneficiar);
    assert.equal(balance.toNumber(), 1750000 * DECIMALS, "Can not be released");
    balance = await token.balanceOf(await crowdsale.secondReserveLockedTeamTokens());
    assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");

  })

  it('secondReserveLockedTeamTokens can be released after time limit', async function () {
    await setState(1);
    await setState(2);
    await increaseTimeTo(duration.years(0.6))
    let beneficiar = accounts[7];
    let lockedTeamTokens = await TokenTimelock.at(await crowdsale.secondReserveLockedTeamTokens()); 
    await crowdsale.setSecondReserveAddress(beneficiar, {from: crowdsaleOwner});
    
    let balance = await token.balanceOf(beneficiar);
    assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
    balance = await token.balanceOf(await crowdsale.secondReserveLockedTeamTokens());
    assert.equal(balance.toNumber(), 1750000 * DECIMALS, "Can not be released");

    await lockedTeamTokens.release({from: crowdsaleOwner});

    balance = await token.balanceOf(beneficiar);
    assert.equal(balance.toNumber(), 1750000 * DECIMALS, "Can not be released");
    balance = await token.balanceOf(await crowdsale.secondReserveLockedTeamTokens());
    assert.equal(balance.toNumber(), 0 * DECIMALS, "Can not be released");
  })

})
